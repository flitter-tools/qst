/***************************************************************************
 *   Copyright (C) 2010 by Petr Vanek                                      *
 *   petr@scribus.info                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include <qtermwidget.h>

#include "properties.h"
#include "config.h"
#include "mainwindow.h"
#include "qterminalapp.h"

Properties * Properties::m_instance = nullptr;


Properties * Properties::Instance(const QString& filename)
{
    if (!m_instance)
        m_instance = new Properties(filename);
    return m_instance;
}

Properties::Properties(const QString& filename)
    : filename(filename)
{
    if (filename.isEmpty())
        m_settings = new QSettings();
    else
        m_settings = new QSettings(filename);
    qDebug("Properties constructor called");
}

Properties::~Properties()
{
    qDebug("Properties destructor called");
    delete m_settings;
    m_instance = nullptr;
}

QFont Properties::defaultFont()
{
    QFont default_font = QApplication::font();
    default_font.setFamily(QLatin1String(DEFAULT_FONT));
    default_font.setPointSize(12);
    default_font.setStyleHint(QFont::TypeWriter);
    return default_font;
}

void Properties::loadSettings()
{
    colorScheme = m_settings->value(QLatin1String("colorScheme"), QLatin1String("Linux")).toString();

    font = QFont(qvariant_cast<QString>(m_settings->value(QLatin1String("fontFamily"), defaultFont().family())),
                 qvariant_cast<int>(m_settings->value(QLatin1String("fontSize"), defaultFont().pointSize())));
    //Legacy font setting
    font = qvariant_cast<QFont>(m_settings->value(QLatin1String("font"), font));

    mainWindowSize = m_settings->value(QLatin1String("MainWindow/size")).toSize();
    mainWindowPosition = m_settings->value(QLatin1String("MainWindow/pos")).toPoint();
    mainWindowState = m_settings->value(QLatin1String("MainWindow/state")).toByteArray();

    historyLimited = m_settings->value(QLatin1String("HistoryLimited"), true).toBool();
    historyLimitedTo = m_settings->value(QLatin1String("HistoryLimitedTo"), 1000).toUInt();

    // sessions
    int size = m_settings->beginReadArray(QLatin1String("Sessions"));
    for (int i = 0; i < size; ++i)
    {
        m_settings->setArrayIndex(i);
        QString name(m_settings->value(QLatin1String("name")).toString());
        if (name.isEmpty())
            continue;
        sessions[name] = m_settings->value(QLatin1String("state")).toString();
    }
    m_settings->endArray();

    terminalMargin = m_settings->value(QLatin1String("TerminalMargin"), 0).toInt();

    termTransparency = m_settings->value(QLatin1String("TerminalTransparency"), 0).toInt();

    /* default to Right. see qtermwidget.h */
    scrollBarPos = m_settings->value(QLatin1String("ScrollbarPosition"), 2).toInt();
    /* default to North. I'd prefer South but North is standard (they say) */
    tabsPos = m_settings->value(QLatin1String("TabsPosition"), 0).toInt();
    /* default to BlockCursor */
    keyboardCursorShape = m_settings->value(QLatin1String("KeyboardCursorShape"), 0).toInt();
    hideTabBarWithOneTab = m_settings->value(QLatin1String("HideTabBarWithOneTab"), false).toBool();
    m_motionAfterPaste = m_settings->value(QLatin1String("MotionAfterPaste"), 0).toInt();

    /* fixed tab width */
    fixedTabWidth = m_settings->value(QLatin1String("FixedTabWidth"), true).toBool();
    fixedTabWidthValue = m_settings->value(QLatin1String("FixedTabWidthValue"), 500).toInt();
    tabHeight = m_settings->value(QLatin1String("TabHeight"), 30).toInt();
    showCloseTabButton = m_settings->value(QLatin1String("ShowCloseTabButton"), true).toBool();

    /* toggles */
    tabBarless = m_settings->value(QLatin1String("TabBarless"), false).toBool();
    menuVisible = m_settings->value(QLatin1String("MenuVisible"), true).toBool();
    saveSizeOnExit = m_settings->value(QLatin1String("SaveSizeOnExit"), true).toBool();
    savePosOnExit = m_settings->value(QLatin1String("SavePosOnExit"), true).toBool();
    useCWD = m_settings->value(QLatin1String("UseCWD"), false).toBool();
    term = m_settings->value(QLatin1String("Term"), QLatin1String("xterm-256color")).toString();

    terminalsPreset = m_settings->value(QLatin1String("TerminalsPreset"), 0).toInt();

    changeWindowTitle = m_settings->value(QLatin1String("ChangeWindowTitle"), true).toBool();
    changeWindowIcon = m_settings->value(QLatin1String("ChangeWindowIcon"), true).toBool();

    confirmMultilinePaste = m_settings->value(QLatin1String("ConfirmMultilinePaste"), false).toBool();
    trimPastedTrailingNewlines = m_settings->value(QLatin1String("TrimPastedTrailingNewlines"), false).toBool();

    windowMaximized = m_settings->value(QLatin1String("LastWindowMaximized"), false).toBool();
}

void Properties::saveSettings()
{
    m_settings->setValue(QLatin1String("colorScheme"), colorScheme);
    m_settings->setValue(QLatin1String("fontFamily"), font.family());
    m_settings->setValue(QLatin1String("fontSize"), font.pointSize());
    //Clobber legacy setting
    m_settings->remove(QLatin1String("font"));

    m_settings->beginGroup(QLatin1String("Shortcuts"));
    MainWindow *mainWindow = QTerminalApp::Instance()->getWindowList()[0];
    if (mainWindow == nullptr)
    {
        qDebug() << "Mainwindow is nullptr!";
        return;
    }

    QMapIterator< QString, QAction * > it(mainWindow->leaseActions());
    while( it.hasNext() )
    {
        it.next();
        QStringList sequenceStrings;
        const auto shortcuts = it.value()->shortcuts();
        for (const QKeySequence &shortcut : shortcuts)
            sequenceStrings.append(shortcut.toString());
        m_settings->setValue(it.key(), sequenceStrings.join(QLatin1Char('|')));
    }
    m_settings->endGroup();

    m_settings->setValue(QLatin1String("MainWindow/size"), mainWindowSize);
    m_settings->setValue(QLatin1String("MainWindow/pos"), mainWindowPosition);
    m_settings->setValue(QLatin1String("MainWindow/state"), mainWindowState);

    m_settings->setValue(QLatin1String("HistoryLimited"), historyLimited);
    m_settings->setValue(QLatin1String("HistoryLimitedTo"), historyLimitedTo);

    // sessions
    m_settings->beginWriteArray(QLatin1String("Sessions"));
    int i = 0;
    Sessions::iterator sit = sessions.begin();
    while (sit != sessions.end())
    {
        m_settings->setArrayIndex(i);
        m_settings->setValue(QLatin1String("name"), sit.key());
        m_settings->setValue(QLatin1String("state"), sit.value());
        ++sit;
        ++i;
    }
    m_settings->endArray();

    m_settings->setValue(QLatin1String("TerminalMargin"), terminalMargin);
    m_settings->setValue(QLatin1String("TerminalTransparency"), termTransparency);
    m_settings->setValue(QLatin1String("ScrollbarPosition"), scrollBarPos);
    m_settings->setValue(QLatin1String("TabsPosition"), tabsPos);
    m_settings->setValue(QLatin1String("KeyboardCursorShape"), keyboardCursorShape);
    m_settings->setValue(QLatin1String("HideTabBarWithOneTab"), hideTabBarWithOneTab);
    m_settings->setValue(QLatin1String("MotionAfterPaste"), m_motionAfterPaste);

    m_settings->setValue(QLatin1String("FixedTabWidth"), fixedTabWidth);
    m_settings->setValue(QLatin1String("FixedTabWidthValue"), fixedTabWidthValue);
    m_settings->setValue(QLatin1String("TabHeight"), tabHeight);
    m_settings->setValue(QLatin1String("ShowCloseTabButton"), showCloseTabButton);

    m_settings->setValue(QLatin1String("TabBarless"), tabBarless);
    m_settings->setValue(QLatin1String("MenuVisible"), menuVisible);
    m_settings->setValue(QLatin1String("SavePosOnExit"), savePosOnExit);
    m_settings->setValue(QLatin1String("SaveSizeOnExit"), saveSizeOnExit);
    m_settings->setValue(QLatin1String("UseCWD"), useCWD);
    m_settings->setValue(QLatin1String("Term"), term);

    m_settings->setValue(QLatin1String("TerminalsPreset"), terminalsPreset);

    m_settings->setValue(QLatin1String("ChangeWindowTitle"), changeWindowTitle);
    m_settings->setValue(QLatin1String("ChangeWindowIcon"), changeWindowIcon);

    m_settings->setValue(QLatin1String("ConfirmMultilinePaste"), confirmMultilinePaste);
    m_settings->setValue(QLatin1String("TrimPastedTrailingNewlines"), trimPastedTrailingNewlines);

    m_settings->setValue(QLatin1String("LastWindowMaximized"), windowMaximized);
}


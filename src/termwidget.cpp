/***************************************************************************
 *   Copyright (C) 2010 by Petr Vanek                                      *
 *   petr@scribus.info                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include <QMenu>
#include <QVBoxLayout>
#include <QPainter>
#include <QDesktopServices>
#include <QMessageBox>
#include <QAbstractButton>
#include <QMouseEvent>

#include "mainwindow.h"
#include "termwidget.h"
#include "config.h"
#include "properties.h"
#include "qterminalapp.h"

static int TermWidgetCount = 0;


TermWidgetImp::TermWidgetImp(TerminalConfig &cfg, QWidget * parent)
    : QTermWidget(0, parent)
{
    TermWidgetCount++;
    QString name(QStringLiteral("TermWidget_%1"));
    setObjectName(name.arg(TermWidgetCount));

    setFlowControlEnabled(FLOW_CONTROL_ENABLED);
    setFlowControlWarningEnabled(FLOW_CONTROL_WARNING_ENABLED);

    propertiesChanged();

    setHistorySize(5000);

    setWorkingDirectory(cfg.getWorkingDirectory());

    QString shell = cfg.getShell();
    if (!shell.isEmpty())
    {
        qDebug() << "Shell program:" << shell;
        QStringList parts = shell.split(QRegExp(QStringLiteral("\\s+")), QString::SkipEmptyParts);
        qDebug() << parts;
        setShellProgram(parts.at(0));
        parts.removeAt(0);
        if (parts.count())
            setArgs(parts);
    }

    setMotionAfterPasting(Properties::Instance()->m_motionAfterPaste);

    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, &QWidget::customContextMenuRequested,
            this, &TermWidgetImp::customContextMenuCall);

    connect(this, &QTermWidget::urlActivated, this, &TermWidgetImp::activateUrl);

    startShellProgram();
}

void TermWidgetImp::propertiesChanged()
{
    setMargin(Properties::Instance()->terminalMargin);
    setColorScheme(Properties::Instance()->colorScheme);
    setTerminalFont(Properties::Instance()->font);
    setMotionAfterPasting(Properties::Instance()->m_motionAfterPaste);

    if (Properties::Instance()->historyLimited)
    {
        setHistorySize(Properties::Instance()->historyLimitedTo);
    }
    else
    {
        // Unlimited history
        setHistorySize(-1);
    }

    setKeyBindings(QLatin1String("default"));
    setTerminalOpacity(1.0 - Properties::Instance()->termTransparency/100.0);

    /* be consequent with qtermwidget.h here */
    switch(Properties::Instance()->scrollBarPos) {
    case 0:
        setScrollBarPosition(QTermWidget::NoScrollBar);
        break;
    case 1:
        setScrollBarPosition(QTermWidget::ScrollBarLeft);
        break;
    case 2:
    default:
        setScrollBarPosition(QTermWidget::ScrollBarRight);
        break;
    }

    switch(Properties::Instance()->keyboardCursorShape) {
    case 1:
        setKeyboardCursorShape(QTermWidget::KeyboardCursorShape::UnderlineCursor);
        break;
    case 2:
        setKeyboardCursorShape(QTermWidget::KeyboardCursorShape::IBeamCursor);
        break;
    default:
    case 0:
        setKeyboardCursorShape(QTermWidget::KeyboardCursorShape::BlockCursor);
        break;
    }

    update();
}

void TermWidgetImp::customContextMenuCall(const QPoint & pos)
{
    QMenu menu;
    QMap<QString, QAction*> actions = findParent<MainWindow>(this)->leaseActions();

    QList<QAction*> extraActions = filterActions(pos);
    for (auto& action : extraActions)
    {
        menu.addAction(action);
    }

    if (!actions.isEmpty())
    {
        menu.addSeparator();
    }

    menu.addAction(actions[QStringLiteral(CLEAR_TERMINAL)]);
    menu.addAction(actions[QStringLiteral(SPLIT_HORIZONTAL)]);
    menu.addAction(actions[QStringLiteral(SPLIT_VERTICAL)]);
    // warning TODO/FIXME: disable the action when there is only one terminal
    menu.addAction(actions[QStringLiteral(SUB_COLLAPSE)]);
    menu.addSeparator();
    menu.addAction(actions[QStringLiteral(PREFERENCES)]);
    menu.exec(mapToGlobal(pos));
}

void TermWidgetImp::zoomIn()
{
    emit QTermWidget::zoomIn();
// note: do not save zoom here due the #74 Zoom reset option resets font back to Monospace
//    Properties::Instance()->font = getTerminalFont();
//    Properties::Instance()->saveSettings();
}

void TermWidgetImp::zoomOut()
{
    emit QTermWidget::zoomOut();
// note: do not save zoom here due the #74 Zoom reset option resets font back to Monospace
//    Properties::Instance()->font = getTerminalFont();
//    Properties::Instance()->saveSettings();
}

void TermWidgetImp::zoomReset()
{
// note: do not save zoom here due the #74 Zoom reset option resets font back to Monospace
//    Properties::Instance()->font = Properties::Instance()->font;
    setTerminalFont(Properties::Instance()->font);
//    Properties::Instance()->saveSettings();
}

void TermWidgetImp::activateUrl(const QUrl & url, bool fromContextMenu) {
    if (QApplication::keyboardModifiers() & Qt::ControlModifier || fromContextMenu) {
        QDesktopServices::openUrl(url);
    }
}

void TermWidgetImp::pasteSelection()
{
    paste(QClipboard::Selection);
}

void TermWidgetImp::pasteClipboard()
{
    paste(QClipboard::Clipboard);
}

void TermWidgetImp::paste(QClipboard::Mode mode)
{
    // Paste Clipboard by simulating keypress events
    QString text = QApplication::clipboard()->text(mode);
    if ( ! text.isEmpty() )
    {
        text.replace(QLatin1String("\r\n"), QLatin1String("\n"));
        text.replace(QLatin1Char('\n'), QLatin1Char('\r'));
        QString trimmedTrailingNl(text);
        trimmedTrailingNl.replace(QRegExp(QStringLiteral("\\r+$")), QString());
        bool isMultiline = trimmedTrailingNl.contains(QLatin1Char('\r'));
        if (!isMultiline && Properties::Instance()->trimPastedTrailingNewlines)
        {
            text = trimmedTrailingNl;
        }
        if (Properties::Instance()->confirmMultilinePaste)
        {
            if (text.contains(QLatin1Char('\r')) && Properties::Instance()->confirmMultilinePaste)
            {
                QMessageBox confirmation(this);
                confirmation.setWindowTitle(tr("Paste multiline text"));
                confirmation.setText(tr("Are you sure you want to paste this text?"));
                confirmation.setDetailedText(text);
                confirmation.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
                // Click "Show details..." to show those by default
                const auto buttons = confirmation.buttons();
                for( QAbstractButton * btn : buttons )
                {
                    if (confirmation.buttonRole(btn) == QMessageBox::ActionRole && btn->text() == QMessageBox::tr("Show Details..."))
                    {
                        btn->clicked();
                        break;
                    }
                }
                confirmation.setDefaultButton(QMessageBox::Yes);
                confirmation.exec();
                if (confirmation.standardButton(confirmation.clickedButton()) != QMessageBox::Yes)
                {
                    return;
                }
            }
        }

        bracketText(text);
        sendText(text);
    }
}

bool TermWidget::eventFilter(QObject * /*obj*/, QEvent * ev)
{
    if (ev->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent *mev = static_cast<QMouseEvent *>(ev);
        if ( mev->button() == Qt::MidButton )
        {
            impl()->pasteSelection();
            return true;
        }
    }
    return false;
}

TermWidget::TermWidget(TerminalConfig &cfg, QWidget * parent)
    : QWidget(parent)
{
    m_border = palette().color(QPalette::Window);
    m_term = new TermWidgetImp(cfg, this);
    setFocusProxy(m_term);

    m_layout = new QVBoxLayout;
    setLayout(m_layout);

    m_layout->addWidget(m_term);
    const auto objs = m_term->children();
    for (QObject *o : objs)
    {
        // Find TerminalDisplay
        if (!o->isWidgetType() || qobject_cast<QWidget*>(o)->isHidden())
            continue;
        o->installEventFilter(this);
    }


    propertiesChanged();

    connect(m_term, &QTermWidget::finished, this, &TermWidget::finished);
    connect(m_term, &QTermWidget::termGetFocus, this, &TermWidget::term_termGetFocus);
    connect(m_term, &QTermWidget::termLostFocus, this, &TermWidget::term_termLostFocus);
    connect(m_term, &QTermWidget::titleChanged, this, [this] { emit termTitleChanged(m_term->title(), m_term->icon()); });
}

void TermWidget::propertiesChanged()
{
    m_layout->setContentsMargins(0, 0, 0, 0);
    m_term->propertiesChanged();
}

void TermWidget::term_termGetFocus()
{
    emit termGetFocus(this);
    update();
}

void TermWidget::term_termLostFocus()
{
    m_border = palette().color(QPalette::Window);
    update();
}

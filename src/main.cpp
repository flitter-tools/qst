/***************************************************************************
 *   Copyright (C) 2006 by Vladimir Kuznetsov                              *
 *   vovanec@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include <QApplication>
#include <QCommandLineParser>

#include "mainwindow.h"
#include "qterminalapp.h"
#include "terminalconfig.h"

QTerminalApp * QTerminalApp::m_instance = nullptr;

int main(int argc, char *argv[])
{
    QApplication::setApplicationName(QStringLiteral("qst"));
    QApplication::setApplicationVersion(QStringLiteral(QTERMINAL_VERSION));
    QApplication::setOrganizationDomain(QStringLiteral("flitter"));
    QSettings::setDefaultFormat(QSettings::IniFormat);

    QTerminalApp *app = QTerminalApp::Instance(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription(QStringLiteral("QST -- fork of QTerminal."));
    parser.addPositionalArgument(QStringLiteral("command"), QStringLiteral("Execute command instead of shell."), QStringLiteral("command"));
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOption(QCommandLineOption({QStringLiteral("e"), QStringLiteral("execute")},
                                        QStringLiteral("Execute command instead of shell."), QStringLiteral("command")));
    parser.addOption(QCommandLineOption({QStringLiteral("p"), QStringLiteral("profile")},
                                        QStringLiteral("Load QST with specific options."), QStringLiteral("file")));
    parser.addOption(QCommandLineOption({QStringLiteral("w"), QStringLiteral("workdir")},
                                        QStringLiteral("Start session with specified work directory."), QStringLiteral("dir")));
    parser.addOption(QCommandLineOption({QStringLiteral("g"), QStringLiteral("geometry")},
                                        QStringLiteral("Set window geometry."), QStringLiteral("wxh-x,y")));
    parser.addOption(QCommandLineOption({QStringLiteral("t"), QStringLiteral("title")},
                                        QStringLiteral("Set window title."), QStringLiteral("string")));
    parser.process(app->arguments());

    QString workdir, shell_command, geometry, title;

    if (app->arguments().count() > 1)
    {
        if (app->arguments()[1][0] == QLatin1Char('-'))
        {
            if (parser.isSet(QStringLiteral("profile")) || parser.isSet(QStringLiteral("p")))
                Properties::Instance(parser.value(QStringLiteral("profile")));

            if (parser.isSet(QStringLiteral("workdir")) || parser.isSet(QStringLiteral("w")))
                workdir = parser.value(QStringLiteral("workdir"));

            if (parser.isSet(QStringLiteral("execute")) || parser.isSet(QStringLiteral("e")))
                shell_command = parser.value(QStringLiteral("execute"));

            if (parser.isSet(QStringLiteral("geometry")) || parser.isSet(QStringLiteral("g")))
                geometry = parser.value(QStringLiteral("geometry"));

            if (parser.isSet(QStringLiteral("title")) || parser.isSet(QStringLiteral("t")))
                title = parser.value(QStringLiteral("title"));
        }
        else
        {
            // do not eat remining arguments
            for (int i = 1; i < app->arguments().count(); ++i)
                shell_command += app->arguments()[i];
        }
    }

    Properties::Instance()->loadSettings();

    qputenv("TERM", Properties::Instance()->term.toLatin1());

    if (workdir.isEmpty())
        workdir = QDir::currentPath();
    app->setWorkingDirectory(workdir);

    // translations
    QString fname = QString::fromLatin1("qterminal_%1.qm").arg(QLocale::system().name().left(2));
    QTranslator translator;
    translator.load(fname, QStringLiteral("/usr/share/qst/translations"));
    app->installTranslator(&translator);

    TerminalConfig initConfig = TerminalConfig(workdir, shell_command, title, geometry);
    app->newWindow(initConfig)->show();

    int ret = app->exec();
    delete Properties::Instance();
    app->cleanup();

    return ret;
}

MainWindow *QTerminalApp::newWindow(TerminalConfig &cfg)
{
    MainWindow *window = nullptr;
    window = new MainWindow(cfg);
    return window;
}

QTerminalApp *QTerminalApp::Instance()
{
    Q_ASSERT(m_instance != nullptr);
    return m_instance;
}

QTerminalApp *QTerminalApp::Instance(int &argc, char **argv)
{
    Q_ASSERT(m_instance == nullptr);
    m_instance = new QTerminalApp(argc, argv);
    return m_instance;
}

QTerminalApp::QTerminalApp(int &argc, char **argv)
    :QApplication(argc, argv)
{
}

QString QTerminalApp::getWorkingDirectory() const
{
    return m_workDir;
}

void QTerminalApp::setWorkingDirectory(const QString &wd)
{
    m_workDir = wd;
}

void QTerminalApp::cleanup() {
    delete m_instance;
    m_instance = nullptr;
}

void QTerminalApp::addWindow(MainWindow *window)
{
    m_windowList.append(window);
}

void QTerminalApp::removeWindow(MainWindow *window)
{
    m_windowList.removeOne(window);
}

QList<MainWindow *> QTerminalApp::getWindowList() const
{
    return m_windowList;
}


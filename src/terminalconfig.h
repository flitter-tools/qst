
#ifndef TERMINALCONFIG_H
#define TERMINALCONFIG_H

#include <QHash>
#include <QString>
#include <QVariant>

class TermWidget;

class TerminalConfig
{
    public:
        TerminalConfig(const QString &wdir, const QString &shell, const QString &title, const QString &geometry);
        TerminalConfig(const TerminalConfig &cfg);
        TerminalConfig();

        QString getWorkingDirectory();
        QString getShell();
        QString getTitle();
        QString getGeometry();

        void setWorkingDirectory(const QString &val);
        void setShell(const QString &val);
        void provideCurrentDirectory(const QString &val);

    private:
    	// True when 
    	QString m_currentDirectory;
    	QString m_workingDirectory;
        QString m_shell;
        QString m_title;
        QString m_geometry;
};

#endif

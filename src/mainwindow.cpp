/***************************************************************************
 *   Copyright (C) 2006 by Vladimir Kuznetsov                              *
 *   vovanec@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include <QDockWidget>
#include <QDesktopWidget>
#include <QToolButton>
#include <QMessageBox>
#include <functional>

#include "terminalconfig.h"
#include "mainwindow.h"
#include "tabwidget.h"
#include "termwidgetholder.h"
#include "config.h"
#include "properties.h"
#include "propertiesdialog.h"
#include "qterminalapp.h"

typedef std::function<bool(MainWindow&)> checkfn;
Q_DECLARE_METATYPE(checkfn)

// TODO/FXIME: probably remove. QSS makes it unusable on mac...
#define QSS_DROP    "MainWindow {border: 1px solid rgba(0, 0, 0, 50%);}\n"

MainWindow::MainWindow(TerminalConfig &cfg,
                       QWidget * parent,
                       Qt::WindowFlags f)
    : QMainWindow(parent,f),
      tabPosition(nullptr),
      scrollBarPosition(nullptr),
      keyboardCursorShape(nullptr),
      tabPosMenu(nullptr),
      scrollPosMenu(nullptr),
      keyboardCursorShapeMenu(nullptr),
      settingOwner(nullptr),
      presetsMenu(nullptr),
      m_config(cfg)
{
    m_removeFinished = false;
    QTerminalApp::Instance()->addWindow(this);
    // We want terminal translucency...
    setAttribute(Qt::WA_TranslucentBackground);
    // ... but neither a fully transparent nor a flat menubar
    // with styles that have translucency and/or gradient.
    setAttribute(Qt::WA_NoSystemBackground, false);

    // This is useless, Qt always delete mainwindow due to out of scope
    // also, we're alredy have deletion of this window in destructor
    // so I comment this to prevent double free
    //setAttribute(Qt::WA_DeleteOnClose);

    setupUi(this);

    connect(actAbout, &QAction::triggered, this, &MainWindow::actAbout_triggered);
    connect(actAboutQt, &QAction::triggered, qApp, &QApplication::aboutQt);

    setContentsMargins(0, 0, 0, 0);

    if (m_config.getGeometry().isEmpty())
    {
        if (Properties::Instance()->saveSizeOnExit)
            resize(Properties::Instance()->mainWindowSize);

        if (Properties::Instance()->savePosOnExit)
            move(Properties::Instance()->mainWindowPosition);
    }
    else
    {
        QStringList sizes = m_config.getGeometry().split(QLatin1Char('-'));
        QStringList wh = sizes[0].split(QLatin1Char('x'));
        QStringList xy = sizes[1].split(QLatin1Char(','));
        int w = wh[0].toInt();
        int h = wh[1].toInt();
        int x = xy[0].toInt();
        int y = xy[1].toInt();
        resize(w, h);
        move(x, y);
    }

    restoreState(Properties::Instance()->mainWindowState);

    consoleTabulator->setAutoFillBackground(true);
    connect(consoleTabulator, &TabWidget::closeTabNotification, this, &MainWindow::testClose);
    consoleTabulator->setTabPosition(static_cast<QTabWidget::TabPosition>(Properties::Instance()->tabsPos));
    //consoleTabulator->setShellProgram(command);

    // apply props
    propertiesChanged();

    setupCustomDirs();

    connect(consoleTabulator, &TabWidget::currentTitleChanged, this, &MainWindow::onCurrentTitleChanged);
    connect(menu_Actions, &QMenu::aboutToShow, this, &MainWindow::updateDisabledActions);

    /* The tab should be added after all changes are made to
       the main window; otherwise, the initial prompt might
       get jumbled because of changes in internal geometry. */
    consoleTabulator->addNewTab(m_config);
}

void MainWindow::rebuildActions()
{
    // Delete all setting-related QObjects
    delete settingOwner;
    settingOwner = new QWidget(this);
    settingOwner->setGeometry(0,0,0,0);

    // Then create them again
    setup_FileMenu_Actions();
    setup_ActionsMenu_Actions();
    setup_ViewMenu_Actions();
}

MainWindow::~MainWindow()
{
    QTerminalApp::Instance()->removeWindow(this);
}

void MainWindow::setup_Action(const char *name, QAction *action, const char *defaultShortcut, const QObject *receiver,
                              const char *slot, QMenu *menu, const QVariant &data)
{
    QSettings settings;
    settings.beginGroup(QStringLiteral("Shortcuts"));

    QList<QKeySequence> shortcuts;

    actions[QLatin1String(name)] = action;
    const auto sequences = settings.value(QLatin1String(name), QLatin1String(defaultShortcut)).toString().split(QLatin1Char('|'));
    for (const QString &sequenceString : sequences)
        shortcuts.append(QKeySequence::fromString(sequenceString));
    actions[QLatin1String(name)]->setShortcuts(shortcuts);
    actions[QLatin1String(name)]->setShortcutContext(Qt::WidgetWithChildrenShortcut);

    if (receiver)
    {
        connect(actions[QLatin1String(name)], SIGNAL(triggered(bool)), receiver, slot);
        addAction(actions[QLatin1String(name)]);
    }

    if (menu)
        menu->addAction(actions[QLatin1String(name)]);

    if (!data.isNull())
        actions[QLatin1String(name)]->setData(data);
}

void MainWindow::setup_ActionsMenu_Actions()
{
    QVariant data;

    const checkfn checkTabs = &MainWindow::hasMultipleTabs;
    const checkfn checkSubterminals = &MainWindow::hasMultipleSubterminals;

    menu_Actions->clear();

    setup_Action(CLEAR_TERMINAL, new QAction(QIcon::fromTheme(QStringLiteral("edit-clear")), tr("&Clear Active Terminal"), settingOwner),
                 CLEAR_TERMINAL_SHORTCUT, consoleTabulator, SLOT(clearActiveTerminal()), menu_Actions);

    menu_Actions->addSeparator();

    data.setValue(checkTabs);

    setup_Action(TAB_NEXT, new QAction(QIcon::fromTheme(QStringLiteral("go-next")), tr("&Next Tab"), settingOwner),
                 TAB_NEXT_SHORTCUT, consoleTabulator, SLOT(switchToRight()), menu_Actions, data);

    setup_Action(TAB_PREV, new QAction(QIcon::fromTheme(QStringLiteral("go-previous")), tr("&Previous Tab"), settingOwner),
                 TAB_PREV_SHORTCUT, consoleTabulator, SLOT(switchToLeft()), menu_Actions, data);

    setup_Action(TAB_PREV_HISTORY, new QAction(tr("&Previous Tab in History"), settingOwner),
                 TAB_PREV_HISTORY_SHORTCUT, consoleTabulator, SLOT(switchToPrev()), menu_Actions, data);

    setup_Action(TAB_NEXT_HISTORY, new QAction(tr("&Next Tab in History"), settingOwner),
                 TAB_NEXT_HISTORY_SHORTCUT, consoleTabulator, SLOT(switchToNext()), menu_Actions, data);

    setup_Action(MOVE_LEFT, new QAction(tr("Move Tab &Left"), settingOwner),
                 MOVE_LEFT_SHORTCUT, consoleTabulator, SLOT(moveLeft()), menu_Actions, data);

    setup_Action(MOVE_RIGHT, new QAction(tr("Move Tab &Right"), settingOwner),
                 MOVE_RIGHT_SHORTCUT, consoleTabulator, SLOT(moveRight()), menu_Actions, data);

    menu_Actions->addSeparator();

    setup_Action(SPLIT_HORIZONTAL, new QAction(tr("Split Terminal &Horizontally"), settingOwner),
                 nullptr, consoleTabulator, SLOT(splitHorizontally()), menu_Actions);

    setup_Action(SPLIT_VERTICAL, new QAction(tr("Split Terminal &Vertically"), settingOwner),
                 nullptr, consoleTabulator, SLOT(splitVertically()), menu_Actions);

    data.setValue(checkSubterminals);

    setup_Action(SUB_COLLAPSE, new QAction(tr("&Collapse Subterminal"), settingOwner),
                 nullptr, consoleTabulator, SLOT(splitCollapse()), menu_Actions, data);

    setup_Action(SUB_TOP, new QAction(QIcon::fromTheme(QStringLiteral("go-up")), tr("&Top Subterminal"), settingOwner),
                 SUB_TOP_SHORTCUT, consoleTabulator, SLOT(switchTopSubterminal()), menu_Actions, data);

    setup_Action(SUB_BOTTOM, new QAction(QIcon::fromTheme(QStringLiteral("go-down")), tr("&Bottom Subterminal"), settingOwner),
                 SUB_BOTTOM_SHORTCUT, consoleTabulator, SLOT(switchBottomSubterminal()), menu_Actions, data);

    setup_Action(SUB_LEFT, new QAction(QIcon::fromTheme(QStringLiteral("go-previous")), tr("L&eft Subterminal"), settingOwner),
                 SUB_LEFT_SHORTCUT, consoleTabulator, SLOT(switchLeftSubterminal()), menu_Actions, data);

    setup_Action(SUB_RIGHT, new QAction(QIcon::fromTheme(QStringLiteral("go-next")), tr("R&ight Subterminal"), settingOwner),
                 SUB_RIGHT_SHORTCUT, consoleTabulator, SLOT(switchRightSubterminal()), menu_Actions, data);


    menu_Actions->addSeparator();

    // Copy and Paste are only added to the table for the sake of bindings at the moment; there is no Edit menu, only a context menu.
    setup_Action(COPY_SELECTION, new QAction(QIcon::fromTheme(QStringLiteral("edit-copy")), tr("Copy &Selection"), settingOwner),
                 COPY_SELECTION_SHORTCUT, consoleTabulator, SLOT(copySelection()), menu_Edit);

    setup_Action(PASTE_CLIPBOARD, new QAction(QIcon::fromTheme(QStringLiteral("edit-paste")), tr("Paste Clip&board"), settingOwner),
                 PASTE_CLIPBOARD_SHORTCUT, consoleTabulator, SLOT(pasteClipboard()), menu_Edit);

    setup_Action(PASTE_SELECTION, new QAction(QIcon::fromTheme(QStringLiteral("edit-paste")), tr("Paste S&election"), settingOwner),
                 PASTE_SELECTION_SHORTCUT, consoleTabulator, SLOT(pasteSelection()), menu_Edit);

    setup_Action(ZOOM_IN, new QAction(QIcon::fromTheme(QStringLiteral("zoom-in")), tr("Zoom &in"), settingOwner),
                 ZOOM_IN_SHORTCUT, consoleTabulator, SLOT(zoomIn()), menu_Edit);

    setup_Action(ZOOM_OUT, new QAction(QIcon::fromTheme(QStringLiteral("zoom-out")), tr("Zoom &out"), settingOwner),
                 ZOOM_OUT_SHORTCUT, consoleTabulator, SLOT(zoomOut()), menu_Edit);

    setup_Action(ZOOM_RESET, new QAction(QIcon::fromTheme(QStringLiteral("zoom-original")), tr("Zoom rese&t"), settingOwner),
                 ZOOM_RESET_SHORTCUT, consoleTabulator, SLOT(zoomReset()), menu_Edit);

    menu_Actions->addSeparator();

    setup_Action(FIND, new QAction(QIcon::fromTheme(QStringLiteral("edit-find")), tr("&Find..."), settingOwner),
                FIND_SHORTCUT, this, SLOT(find()), menu_Actions);

#if 0
    act = new QAction(this);
    act->setSeparator(true);

    // TODO/FIXME: unimplemented for now
    act = new QAction(tr("&Save Session"), this);
    // do not use sequences for this task - it collides with eg. mc shorcuts
    // and mainly - it's not used too often
    //act->setShortcut(QKeySequence::Save);
    connect(act, SIGNAL(triggered()), consoleTabulator, SLOT(saveSession()));

    act = new QAction(tr("&Load Session"), this);
    // do not use sequences for this task - it collides with eg. mc shorcuts
    // and mainly - it's not used too often
    //act->setShortcut(QKeySequence::Open);
    connect(act, SIGNAL(triggered()), consoleTabulator, SLOT(loadSession()));
#endif

    // Add global rename current session shortcut
    setup_Action(RENAME_SESSION, new QAction(tr("Rename session"), settingOwner),
                 RENAME_SESSION_SHORTCUT, consoleTabulator, SLOT(renameCurrentSession()));
    // this is correct - add action to main window - not to menu

}
void MainWindow::setup_FileMenu_Actions()
{
    menu_File->clear();
    setup_Action(ADD_TAB, new QAction(QIcon::fromTheme(QStringLiteral("list-add")), tr("&New Tab"), settingOwner),
                 ADD_TAB_SHORTCUT, this, SLOT(addNewTab()), menu_File);

    if (presetsMenu == nullptr) {
        presetsMenu = new QMenu(tr("New Tab From &Preset"), this);
        presetsMenu->addAction(QIcon(), tr("1 &Terminal"),
                               this, SLOT(addNewTab()));
        presetsMenu->addAction(QIcon(), tr("2 &Horizontal Terminals"),
                               consoleTabulator, SLOT(preset2Horizontal()));
        presetsMenu->addAction(QIcon(), tr("2 &Vertical Terminals"),
                               consoleTabulator, SLOT(preset2Vertical()));
        presetsMenu->addAction(QIcon(), tr("4 Terminal&s"),
                               consoleTabulator, SLOT(preset4Terminals()));
    }

    menu_File->addMenu(presetsMenu);

    setup_Action(CLOSE_TAB, new QAction(QIcon::fromTheme(QStringLiteral("list-remove")), tr("&Close Tab"), settingOwner),
                 CLOSE_TAB_SHORTCUT, consoleTabulator, SLOT(removeCurrentTab()), menu_File);

    setup_Action(NEW_WINDOW, new QAction(QIcon::fromTheme(QStringLiteral("window-new")), tr("&New Window"), settingOwner),
                 NEW_WINDOW_SHORTCUT, this, SLOT(newTerminalWindow()), menu_File);

    menu_File->addSeparator();

    setup_Action(PREFERENCES, new QAction(tr("&Preferences..."), settingOwner), "", this, SLOT(actProperties_triggered()), menu_File);

    menu_File->addSeparator();

    setup_Action(QUIT, new QAction(QIcon::fromTheme(QStringLiteral("application-exit")), tr("&Quit"), settingOwner), "", this, SLOT(close()), menu_File);
}

void MainWindow::setup_ViewMenu_Actions()
{
    menu_Window->clear();

    QAction *showTabBarAction = new QAction(tr("&Show Tab Bar"), settingOwner);
    //toggleTabbar->setObjectName("toggle_TabBar");
    showTabBarAction->setCheckable(true);
    showTabBarAction->setChecked(!Properties::Instance()->tabBarless);
    setup_Action(SHOW_TAB_BAR, showTabBarAction,
                 nullptr, this, SLOT(toggleTabBar()), menu_Window);
    toggleTabBar();

    QAction *toggleFullscreen = new QAction(tr("Fullscreen"), settingOwner);
    toggleFullscreen->setCheckable(true);
    toggleFullscreen->setChecked(false);
    setup_Action(FULLSCREEN, toggleFullscreen,
                 FULLSCREEN_SHORTCUT, this, SLOT(showFullscreen(bool)), menu_Window);

    menu_Window->addSeparator();

    /* tabs position */
    if (tabPosition == nullptr) {
        tabPosition = new QActionGroup(this);
        QAction *tabBottom = new QAction(tr("&Bottom"), this);
        QAction *tabTop = new QAction(tr("&Top"), this);
        QAction *tabRight = new QAction(tr("&Right"), this);
        QAction *tabLeft = new QAction(tr("&Left"), this);
        tabPosition->addAction(tabTop);
        tabPosition->addAction(tabBottom);
        tabPosition->addAction(tabLeft);
        tabPosition->addAction(tabRight);

        for(int i = 0; i < tabPosition->actions().size(); ++i)
            tabPosition->actions().at(i)->setCheckable(true);
    }


    if( tabPosition->actions().count() > Properties::Instance()->tabsPos )
        tabPosition->actions().at(Properties::Instance()->tabsPos)->setChecked(true);

    connect(tabPosition, &QActionGroup::triggered,
             consoleTabulator, &TabWidget::changeTabPosition);

    if (tabPosMenu == nullptr) {
        tabPosMenu = new QMenu(tr("&Tabs Layout"), menu_Window);
        tabPosMenu->setObjectName(QStringLiteral("tabPosMenu"));

        for(int i=0; i < tabPosition->actions().size(); ++i) {
            tabPosMenu->addAction(tabPosition->actions().at(i));
        }

        connect(menu_Window, &QMenu::hovered,
                this, &MainWindow::updateActionGroup);
    }
    menu_Window->addMenu(tabPosMenu);
    /* */

    /* Scrollbar */
    if (scrollBarPosition == nullptr) {
        scrollBarPosition = new QActionGroup(this);
        QAction *scrollNone = new QAction(tr("&None"), this);
        QAction *scrollRight = new QAction(tr("&Right"), this);
        QAction *scrollLeft = new QAction(tr("&Left"), this);
        /* order of insertion is dep. on QTermWidget::ScrollBarPosition enum */
        scrollBarPosition->addAction(scrollNone);
        scrollBarPosition->addAction(scrollLeft);
        scrollBarPosition->addAction(scrollRight);

        for(int i = 0; i < scrollBarPosition->actions().size(); ++i)
            scrollBarPosition->actions().at(i)->setCheckable(true);

        if( Properties::Instance()->scrollBarPos < scrollBarPosition->actions().size() )
            scrollBarPosition->actions().at(Properties::Instance()->scrollBarPos)->setChecked(true);
        connect(scrollBarPosition, &QActionGroup::triggered,
             consoleTabulator, &TabWidget::changeScrollPosition);

    }
    if (scrollPosMenu == nullptr) {
        scrollPosMenu = new QMenu(tr("S&crollbar Layout"), menu_Window);
        scrollPosMenu->setObjectName(QStringLiteral("scrollPosMenu"));

        for(int i=0; i < scrollBarPosition->actions().size(); ++i) {
            scrollPosMenu->addAction(scrollBarPosition->actions().at(i));
        }
    }

    menu_Window->addMenu(scrollPosMenu);

    /* Keyboard cursor shape */
    if (keyboardCursorShape == nullptr) {
        keyboardCursorShape = new QActionGroup(this);
        QAction *block = new QAction(tr("&BlockCursor"), this);
        QAction *underline = new QAction(tr("&UnderlineCursor"), this);
        QAction *ibeam = new QAction(tr("&IBeamCursor"), this);

        /* order of insertion is dep. on QTermWidget::KeyboardCursorShape enum */
        keyboardCursorShape->addAction(block);
        keyboardCursorShape->addAction(underline);
        keyboardCursorShape->addAction(ibeam);
        for(int i = 0; i < keyboardCursorShape->actions().size(); ++i)
            keyboardCursorShape->actions().at(i)->setCheckable(true);

        if( Properties::Instance()->keyboardCursorShape < keyboardCursorShape->actions().size() )
            keyboardCursorShape->actions().at(Properties::Instance()->keyboardCursorShape)->setChecked(true);

        connect(keyboardCursorShape, &QActionGroup::triggered,
                 consoleTabulator, &TabWidget::changeKeyboardCursorShape);
    }

    if (keyboardCursorShapeMenu == nullptr) {
        keyboardCursorShapeMenu = new QMenu(tr("&Keyboard Cursor Shape"), menu_Window);
        keyboardCursorShapeMenu->setObjectName(QStringLiteral("keyboardCursorShapeMenu"));

        for(int i=0; i < keyboardCursorShape->actions().size(); ++i) {
            keyboardCursorShapeMenu->addAction(keyboardCursorShape->actions().at(i));
        }
    }

    menu_Window->addMenu(keyboardCursorShapeMenu);
}

void MainWindow::setupCustomDirs()
{
    const QSettings settings;
    const QString dir = QFileInfo(settings.fileName()).canonicalPath() + QStringLiteral("/color-schemes/");
    TermWidgetImp::addCustomColorSchemeDir(dir);
}

void MainWindow::on_consoleTabulator_currentChanged(int)
{
}

void MainWindow::toggleTabBar()
{
    Properties::Instance()->tabBarless
            = !actions[QLatin1String(SHOW_TAB_BAR)]->isChecked();
    consoleTabulator->showHideTabBar();
}

void MainWindow::toggleMenu()
{
    m_menuBar->setVisible(!m_menuBar->isVisible());
    Properties::Instance()->menuVisible = m_menuBar->isVisible();
}

void MainWindow::showFullscreen(bool fullscreen)
{
    if(fullscreen)
        setWindowState(windowState() | Qt::WindowFullScreen);
    else
        setWindowState(windowState() & ~Qt::WindowFullScreen);
}

void MainWindow::testClose(bool removeFinished)
{
    m_removeFinished = removeFinished;
    close();
}

void MainWindow::closeEvent(QCloseEvent *ev)
{
    // do not show exit dialog if there is only one tab
    if (consoleTabulator->count() == 1)
    {
        ev->accept();
        return;
    }

    // ask user for cancel only when there is at least one terminal active in this window
    QDialog * dia = new QDialog(this);
    dia->setObjectName(QStringLiteral("exitDialog"));
    dia->setWindowTitle(tr("Exit QST"));

    QDialogButtonBox * buttonBox = new QDialogButtonBox(QDialogButtonBox::Yes | QDialogButtonBox::No, Qt::Horizontal, dia);
    buttonBox->button(QDialogButtonBox::Yes)->setDefault(true);

    connect(buttonBox, &QDialogButtonBox::accepted, dia, &QDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, dia, &QDialog::reject);

    QVBoxLayout * lay = new QVBoxLayout();
    lay->addWidget(new QLabel(tr("Are you sure you want to exit?")));
    lay->addWidget(buttonBox);
    dia->setLayout(lay);

    if (dia->exec() == QDialog::Accepted) {
        Properties::Instance()->mainWindowPosition = pos();
        Properties::Instance()->mainWindowSize = size();
        Properties::Instance()->mainWindowState = saveState();
        Properties::Instance()->windowMaximized = isMaximized();
        Properties::Instance()->saveSettings();
        for (int i = consoleTabulator->count(); i > 0; --i) {
            consoleTabulator->removeTab(i - 1);
        }
        ev->accept();
    } else {
        if(m_removeFinished) {
            QWidget *w = consoleTabulator->widget(consoleTabulator->count()-1);
            consoleTabulator->removeTab(consoleTabulator->count()-1);
            delete w; // delete the widget because the window isn't closed
            m_removeFinished = false;
        }
        ev->ignore();
    }

    dia->deleteLater();
}

void MainWindow::actAbout_triggered()
{
    QMessageBox::about(this, QStringLiteral("QST ") + QLatin1String(QTERMINAL_VERSION), tr("A lightweight multiplatform terminal emulator"));
}

void MainWindow::actProperties_triggered()
{
    PropertiesDialog *p = new PropertiesDialog(this);
    connect(p, &PropertiesDialog::propertiesChanged, this, &MainWindow::propertiesChanged);
    p->exec();
}

void MainWindow::propertiesChanged()
{
    rebuildActions();

    consoleTabulator->setTabPosition(static_cast<QTabWidget::TabPosition>(Properties::Instance()->tabsPos));
    consoleTabulator->propertiesChanged();

    m_menuBar->setVisible(Properties::Instance()->menuVisible);

    onCurrentTitleChanged(consoleTabulator->currentIndex());
}

void MainWindow::updateActionGroup(QAction *a)
{
    if (a->parent()->objectName() == tabPosMenu->objectName()) {
        tabPosition->actions().at(Properties::Instance()->tabsPos)->setChecked(true);
    }
}

void MainWindow::find()
{
    // A bit ugly perhaps with 4 levels of indirection...
    consoleTabulator->terminalHolder()->currentTerminal()->impl()->toggleShowSearchBar();
}


bool MainWindow::event(QEvent *event)
{
    return QMainWindow::event(event);
}

void MainWindow::newTerminalWindow()
{
    TerminalConfig cfg;
    TermWidgetHolder *ch = consoleTabulator->terminalHolder();
    if (ch)
        cfg.provideCurrentDirectory(ch->currentTerminal()->impl()->workingDirectory());

    MainWindow *w = new MainWindow(cfg);
    w->show();
}

void MainWindow::addNewTab()
{
    TerminalConfig cfg;
    if (Properties::Instance()->terminalsPreset == 3)
        consoleTabulator->preset4Terminals();
    else if (Properties::Instance()->terminalsPreset == 2)
        consoleTabulator->preset2Vertical();
    else if (Properties::Instance()->terminalsPreset == 1)
        consoleTabulator->preset2Horizontal();
    else
        consoleTabulator->addNewTab(cfg);
    updateDisabledActions();
}

void MainWindow::onCurrentTitleChanged(int index)
{
    QString title;
    QIcon icon;
    if (index != -1 )
    {
        title = consoleTabulator->tabText(index);
        icon = consoleTabulator->tabIcon(index);
    }
    setWindowTitle(title.isEmpty() || !Properties::Instance()->changeWindowTitle ? QStringLiteral("QST") : title);
    setWindowIcon(icon.isNull() || !Properties::Instance()->changeWindowIcon ? QIcon::fromTheme(QStringLiteral("utilities-terminal")) : icon);
}

bool MainWindow::hasMultipleTabs()
{
    return consoleTabulator->findChildren<TermWidgetHolder*>().count() > 1;
}

bool MainWindow::hasMultipleSubterminals()
{
    return consoleTabulator->terminalHolder()->findChildren<TermWidget*>().count() > 1;
}

void MainWindow::updateDisabledActions()
{
    const QList<QAction*> actions = menu_Actions->actions();
    for (QAction *action : actions) {
        if (!action->data().isNull()) {
            const checkfn check = action->data().value<checkfn>();
            action->setEnabled(check(*this));
        }
    }
}


QMap< QString, QAction * >& MainWindow::leaseActions() {
        return actions;
}

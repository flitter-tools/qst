# QST

Qt Simple Terminal Emulator

Fork of QTerminal with cleanup code, remove unnecessary features. Trying to make feel and look of ST (aka Suckless Terminal).

### Goal
Create similar to ST (in words of memory consume and simpliness) terminal emulator. Some work already done: e.g. remove all C code, less memory consumetion (10Mb vs 15Mb original). Fix couple of "problems".

### What was removed
- Bookmarks
- Ability to set terminal image
- Ability to set window opacity (terminal widget still may be transparent)
- Drop support of Windows and macOS (because fuck them both)
- Some actions from drop-down menu (we have global menu, isn't we?)
- Remove unnecessary and probably harm code

### What was done so far
- Refactoring code
- Refactoring cmake file
- Removed all pure C code
- Allow user to set geometry as option
- Allow user to set title as option
- Terminal multiplexor: set size of borderline to 1px
- Add ability to set tab height
- Annoying close session message only appears if you try to quit terminal and has more then 1 tab open
- Save settings separatly from QTerminal
- Load translation separatly from QTerminal
- Add PKGBUILD

### What was planned
- Dynamic terminal title (based on BASH_COMMAND ?)
- Reduce starting time
- Reduce memory usage

### Overview

QST is a lightweight Qt terminal emulator based on [QTerminal](https://github.com/lxqt/qterminal) and [QTermWidget](https://github.com/lxqt/qtermwidget).

This project is licensed under the terms of the [GPLv2](https://www.gnu.org/licenses/gpl-2.0.en.html) or any later version. See the LICENSE file for the full text of the license.

### Installation

Dependencies are [QTermWidget](https://github.com/lxqt/qtermwidget).

Code configuration is handled by CMake. Building out of source is required. CMake variable `CMAKE_INSTALL_PREFIX` will normally have to be set to `/usr`.

To build run `make`, to install `make install` which accepts variable `DESTDIR` as usual.

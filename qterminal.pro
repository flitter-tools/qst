TARGET = qterminal
TEMPLATE = app
# qt5 only. Please use cmake - it's an official build tool for this software
QT += widgets

CONFIG += link_pkgconfig \
          depend_includepath

PKGCONFIG += qtermwidget5

DEFINES += QTERMINAL_VERSION=\\\"0.9.0\\\"

SOURCES += $$files(src/*.cpp)
HEADERS += $$files(src/*.h)

INCLUDEPATH += src

FORMS += $$files(src/forms/*.ui)

bin.path   = /usr/bin
bin.files   = qterminal

shortcut.path = /usr/share/applications
shortcut.files = qterminal.desktop
    
INSTALLS += bin shortcut
